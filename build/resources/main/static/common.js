"use strict";
(self["webpackChunkstore"] = self["webpackChunkstore"] || []).push([["common"],{

/***/ 1726:
/*!*******************************************************!*\
  !*** ./src/main/webapp/app/config/input.constants.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DATE_FORMAT": () => (/* binding */ DATE_FORMAT),
/* harmony export */   "DATE_TIME_FORMAT": () => (/* binding */ DATE_TIME_FORMAT)
/* harmony export */ });
const DATE_FORMAT = 'YYYY-MM-DD';
const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';


/***/ }),

/***/ 4218:
/*!************************************************************!*\
  !*** ./src/main/webapp/app/config/pagination.constants.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ITEMS_PER_PAGE": () => (/* binding */ ITEMS_PER_PAGE),
/* harmony export */   "ASC": () => (/* binding */ ASC),
/* harmony export */   "DESC": () => (/* binding */ DESC),
/* harmony export */   "SORT": () => (/* binding */ SORT)
/* harmony export */ });
const ITEMS_PER_PAGE = 20;
const ASC = 'asc';
const DESC = 'desc';
const SORT = 'sort';


/***/ }),

/***/ 5929:
/*!**********************************************************!*\
  !*** ./src/main/webapp/app/core/request/request-util.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createRequestOption": () => (/* binding */ createRequestOption)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 3882);

const createRequestOption = (req) => {
    let options = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach((val) => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};


/***/ }),

/***/ 6037:
/*!****************************************************!*\
  !*** ./src/main/webapp/app/core/util/operators.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "isPresent": () => (/* binding */ isPresent),
/* harmony export */   "filterNaN": () => (/* binding */ filterNaN)
/* harmony export */ });
/*
 * Function used to workaround https://github.com/microsoft/TypeScript/issues/16069
 * es2019 alternative `const filteredArr = myArr.flatMap((x) => x ? x : []);`
 */
function isPresent(t) {
    return t !== undefined && t !== null;
}
const filterNaN = (input) => (isNaN(input) ? 0 : input);


/***/ }),

/***/ 9622:
/*!*****************************************************************!*\
  !*** ./src/main/webapp/app/entities/customer/customer.model.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Customer": () => (/* binding */ Customer),
/* harmony export */   "getCustomerIdentifier": () => (/* binding */ getCustomerIdentifier)
/* harmony export */ });
class Customer {
    constructor(id, firstName, lastName, gender, email, phone, addressLine1, addressLine2, city, country, user, orders) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.country = country;
        this.user = user;
        this.orders = orders;
    }
}
function getCustomerIdentifier(customer) {
    return customer.id;
}


/***/ }),

/***/ 6088:
/*!***************************************************************************!*\
  !*** ./src/main/webapp/app/entities/customer/service/customer.service.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomerService": () => (/* binding */ CustomerService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _customer_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../customer.model */ 9622);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2316);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 3882);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class CustomerService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/customers');
    }
    create(customer) {
        return this.http.post(this.resourceUrl, customer, { observe: 'response' });
    }
    update(customer) {
        return this.http.put(`${this.resourceUrl}/${(0,_customer_model__WEBPACK_IMPORTED_MODULE_2__.getCustomerIdentifier)(customer)}`, customer, { observe: 'response' });
    }
    partialUpdate(customer) {
        return this.http.patch(`${this.resourceUrl}/${(0,_customer_model__WEBPACK_IMPORTED_MODULE_2__.getCustomerIdentifier)(customer)}`, customer, {
            observe: 'response',
        });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addCustomerToCollectionIfMissing(customerCollection, ...customersToCheck) {
        const customers = customersToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (customers.length > 0) {
            const customerCollectionIdentifiers = customerCollection.map(customerItem => (0,_customer_model__WEBPACK_IMPORTED_MODULE_2__.getCustomerIdentifier)(customerItem));
            const customersToAdd = customers.filter(customerItem => {
                const customerIdentifier = (0,_customer_model__WEBPACK_IMPORTED_MODULE_2__.getCustomerIdentifier)(customerItem);
                if (customerIdentifier == null || customerCollectionIdentifiers.includes(customerIdentifier)) {
                    return false;
                }
                customerCollectionIdentifiers.push(customerIdentifier);
                return true;
            });
            return [...customersToAdd, ...customerCollection];
        }
        return customerCollection;
    }
}
CustomerService.ɵfac = function CustomerService_Factory(t) { return new (t || CustomerService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
CustomerService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: CustomerService, factory: CustomerService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 9708:
/*!***************************************************************!*\
  !*** ./src/main/webapp/app/entities/invoice/invoice.model.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Invoice": () => (/* binding */ Invoice),
/* harmony export */   "getInvoiceIdentifier": () => (/* binding */ getInvoiceIdentifier)
/* harmony export */ });
class Invoice {
    constructor(id, date, details, status, paymentMethod, paymentDate, paymentAmount, shipments, order) {
        this.id = id;
        this.date = date;
        this.details = details;
        this.status = status;
        this.paymentMethod = paymentMethod;
        this.paymentDate = paymentDate;
        this.paymentAmount = paymentAmount;
        this.shipments = shipments;
        this.order = order;
    }
}
function getInvoiceIdentifier(invoice) {
    return invoice.id;
}


/***/ }),

/***/ 6108:
/*!*************************************************************************!*\
  !*** ./src/main/webapp/app/entities/invoice/service/invoice.service.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InvoiceService": () => (/* binding */ InvoiceService)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 3927);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dayjs */ 160);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _invoice_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../invoice.model */ 9708);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 2316);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ 3882);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);








class InvoiceService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/invoices');
    }
    create(invoice) {
        const copy = this.convertDateFromClient(invoice);
        return this.http
            .post(this.resourceUrl, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    update(invoice) {
        const copy = this.convertDateFromClient(invoice);
        return this.http
            .put(`${this.resourceUrl}/${(0,_invoice_model__WEBPACK_IMPORTED_MODULE_3__.getInvoiceIdentifier)(invoice)}`, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    partialUpdate(invoice) {
        const copy = this.convertDateFromClient(invoice);
        return this.http
            .patch(`${this.resourceUrl}/${(0,_invoice_model__WEBPACK_IMPORTED_MODULE_3__.getInvoiceIdentifier)(invoice)}`, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    find(id) {
        return this.http
            .get(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_2__.createRequestOption)(req);
        return this.http
            .get(this.resourceUrl, { params: options, observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateArrayFromServer(res)));
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addInvoiceToCollectionIfMissing(invoiceCollection, ...invoicesToCheck) {
        const invoices = invoicesToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_1__.isPresent);
        if (invoices.length > 0) {
            const invoiceCollectionIdentifiers = invoiceCollection.map(invoiceItem => (0,_invoice_model__WEBPACK_IMPORTED_MODULE_3__.getInvoiceIdentifier)(invoiceItem));
            const invoicesToAdd = invoices.filter(invoiceItem => {
                const invoiceIdentifier = (0,_invoice_model__WEBPACK_IMPORTED_MODULE_3__.getInvoiceIdentifier)(invoiceItem);
                if (invoiceIdentifier == null || invoiceCollectionIdentifiers.includes(invoiceIdentifier)) {
                    return false;
                }
                invoiceCollectionIdentifiers.push(invoiceIdentifier);
                return true;
            });
            return [...invoicesToAdd, ...invoiceCollection];
        }
        return invoiceCollection;
    }
    convertDateFromClient(invoice) {
        var _a, _b;
        return Object.assign({}, invoice, {
            date: ((_a = invoice.date) === null || _a === void 0 ? void 0 : _a.isValid()) ? invoice.date.toJSON() : undefined,
            paymentDate: ((_b = invoice.paymentDate) === null || _b === void 0 ? void 0 : _b.isValid()) ? invoice.paymentDate.toJSON() : undefined,
        });
    }
    convertDateFromServer(res) {
        if (res.body) {
            res.body.date = res.body.date ? dayjs__WEBPACK_IMPORTED_MODULE_0__(res.body.date) : undefined;
            res.body.paymentDate = res.body.paymentDate ? dayjs__WEBPACK_IMPORTED_MODULE_0__(res.body.paymentDate) : undefined;
        }
        return res;
    }
    convertDateArrayFromServer(res) {
        if (res.body) {
            res.body.forEach((invoice) => {
                invoice.date = invoice.date ? dayjs__WEBPACK_IMPORTED_MODULE_0__(invoice.date) : undefined;
                invoice.paymentDate = invoice.paymentDate ? dayjs__WEBPACK_IMPORTED_MODULE_0__(invoice.paymentDate) : undefined;
            });
        }
        return res;
    }
}
InvoiceService.ɵfac = function InvoiceService_Factory(t) { return new (t || InvoiceService)(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_7__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_4__.ApplicationConfigService)); };
InvoiceService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjectable"]({ token: InvoiceService, factory: InvoiceService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2277:
/*!*********************************************************************************!*\
  !*** ./src/main/webapp/app/entities/product-category/product-category.model.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductCategory": () => (/* binding */ ProductCategory),
/* harmony export */   "getProductCategoryIdentifier": () => (/* binding */ getProductCategoryIdentifier)
/* harmony export */ });
class ProductCategory {
    constructor(id, name, description, products) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.products = products;
    }
}
function getProductCategoryIdentifier(productCategory) {
    return productCategory.id;
}


/***/ }),

/***/ 69:
/*!*******************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/product-category/service/product-category.service.ts ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductCategoryService": () => (/* binding */ ProductCategoryService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _product_category_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../product-category.model */ 2277);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2316);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 3882);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class ProductCategoryService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/product-categories');
    }
    create(productCategory) {
        return this.http.post(this.resourceUrl, productCategory, { observe: 'response' });
    }
    update(productCategory) {
        return this.http.put(`${this.resourceUrl}/${(0,_product_category_model__WEBPACK_IMPORTED_MODULE_2__.getProductCategoryIdentifier)(productCategory)}`, productCategory, { observe: 'response' });
    }
    partialUpdate(productCategory) {
        return this.http.patch(`${this.resourceUrl}/${(0,_product_category_model__WEBPACK_IMPORTED_MODULE_2__.getProductCategoryIdentifier)(productCategory)}`, productCategory, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addProductCategoryToCollectionIfMissing(productCategoryCollection, ...productCategoriesToCheck) {
        const productCategories = productCategoriesToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (productCategories.length > 0) {
            const productCategoryCollectionIdentifiers = productCategoryCollection.map(productCategoryItem => (0,_product_category_model__WEBPACK_IMPORTED_MODULE_2__.getProductCategoryIdentifier)(productCategoryItem));
            const productCategoriesToAdd = productCategories.filter(productCategoryItem => {
                const productCategoryIdentifier = (0,_product_category_model__WEBPACK_IMPORTED_MODULE_2__.getProductCategoryIdentifier)(productCategoryItem);
                if (productCategoryIdentifier == null || productCategoryCollectionIdentifiers.includes(productCategoryIdentifier)) {
                    return false;
                }
                productCategoryCollectionIdentifiers.push(productCategoryIdentifier);
                return true;
            });
            return [...productCategoriesToAdd, ...productCategoryCollection];
        }
        return productCategoryCollection;
    }
}
ProductCategoryService.ɵfac = function ProductCategoryService_Factory(t) { return new (t || ProductCategoryService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
ProductCategoryService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: ProductCategoryService, factory: ProductCategoryService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 9316:
/*!***************************************************************************!*\
  !*** ./src/main/webapp/app/entities/product-order/product-order.model.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductOrder": () => (/* binding */ ProductOrder),
/* harmony export */   "getProductOrderIdentifier": () => (/* binding */ getProductOrderIdentifier)
/* harmony export */ });
class ProductOrder {
    constructor(id, placedDate, status, code, orderItems, invoices, customer) {
        this.id = id;
        this.placedDate = placedDate;
        this.status = status;
        this.code = code;
        this.orderItems = orderItems;
        this.invoices = invoices;
        this.customer = customer;
    }
}
function getProductOrderIdentifier(productOrder) {
    return productOrder.id;
}


/***/ }),

/***/ 3756:
/*!*************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/product-order/service/product-order.service.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductOrderService": () => (/* binding */ ProductOrderService)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 3927);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dayjs */ 160);
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _product_order_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../product-order.model */ 9316);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 2316);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ 3882);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);








class ProductOrderService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/product-orders');
    }
    create(productOrder) {
        const copy = this.convertDateFromClient(productOrder);
        return this.http
            .post(this.resourceUrl, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    update(productOrder) {
        const copy = this.convertDateFromClient(productOrder);
        return this.http
            .put(`${this.resourceUrl}/${(0,_product_order_model__WEBPACK_IMPORTED_MODULE_3__.getProductOrderIdentifier)(productOrder)}`, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    partialUpdate(productOrder) {
        const copy = this.convertDateFromClient(productOrder);
        return this.http
            .patch(`${this.resourceUrl}/${(0,_product_order_model__WEBPACK_IMPORTED_MODULE_3__.getProductOrderIdentifier)(productOrder)}`, copy, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    find(id) {
        return this.http
            .get(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateFromServer(res)));
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_2__.createRequestOption)(req);
        return this.http
            .get(this.resourceUrl, { params: options, observe: 'response' })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => this.convertDateArrayFromServer(res)));
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addProductOrderToCollectionIfMissing(productOrderCollection, ...productOrdersToCheck) {
        const productOrders = productOrdersToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_1__.isPresent);
        if (productOrders.length > 0) {
            const productOrderCollectionIdentifiers = productOrderCollection.map(productOrderItem => (0,_product_order_model__WEBPACK_IMPORTED_MODULE_3__.getProductOrderIdentifier)(productOrderItem));
            const productOrdersToAdd = productOrders.filter(productOrderItem => {
                const productOrderIdentifier = (0,_product_order_model__WEBPACK_IMPORTED_MODULE_3__.getProductOrderIdentifier)(productOrderItem);
                if (productOrderIdentifier == null || productOrderCollectionIdentifiers.includes(productOrderIdentifier)) {
                    return false;
                }
                productOrderCollectionIdentifiers.push(productOrderIdentifier);
                return true;
            });
            return [...productOrdersToAdd, ...productOrderCollection];
        }
        return productOrderCollection;
    }
    convertDateFromClient(productOrder) {
        var _a;
        return Object.assign({}, productOrder, {
            placedDate: ((_a = productOrder.placedDate) === null || _a === void 0 ? void 0 : _a.isValid()) ? productOrder.placedDate.toJSON() : undefined,
        });
    }
    convertDateFromServer(res) {
        if (res.body) {
            res.body.placedDate = res.body.placedDate ? dayjs__WEBPACK_IMPORTED_MODULE_0__(res.body.placedDate) : undefined;
        }
        return res;
    }
    convertDateArrayFromServer(res) {
        if (res.body) {
            res.body.forEach((productOrder) => {
                productOrder.placedDate = productOrder.placedDate ? dayjs__WEBPACK_IMPORTED_MODULE_0__(productOrder.placedDate) : undefined;
            });
        }
        return res;
    }
}
ProductOrderService.ɵfac = function ProductOrderService_Factory(t) { return new (t || ProductOrderService)(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_7__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_4__.ApplicationConfigService)); };
ProductOrderService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjectable"]({ token: ProductOrderService, factory: ProductOrderService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 5262:
/*!***************************************************************!*\
  !*** ./src/main/webapp/app/entities/product/product.model.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Product": () => (/* binding */ Product),
/* harmony export */   "getProductIdentifier": () => (/* binding */ getProductIdentifier)
/* harmony export */ });
class Product {
    constructor(id, name, description, price, sizeproduct, imageContentType, image, productCategory) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.sizeproduct = sizeproduct;
        this.imageContentType = imageContentType;
        this.image = image;
        this.productCategory = productCategory;
    }
}
function getProductIdentifier(product) {
    return product.id;
}


/***/ }),

/***/ 1817:
/*!*************************************************************************!*\
  !*** ./src/main/webapp/app/entities/product/service/product.service.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductService": () => (/* binding */ ProductService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _product_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../product.model */ 5262);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2316);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 3882);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class ProductService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/products');
    }
    create(product) {
        return this.http.post(this.resourceUrl, product, { observe: 'response' });
    }
    update(product) {
        return this.http.put(`${this.resourceUrl}/${(0,_product_model__WEBPACK_IMPORTED_MODULE_2__.getProductIdentifier)(product)}`, product, { observe: 'response' });
    }
    partialUpdate(product) {
        return this.http.patch(`${this.resourceUrl}/${(0,_product_model__WEBPACK_IMPORTED_MODULE_2__.getProductIdentifier)(product)}`, product, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addProductToCollectionIfMissing(productCollection, ...productsToCheck) {
        const products = productsToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (products.length > 0) {
            const productCollectionIdentifiers = productCollection.map(productItem => (0,_product_model__WEBPACK_IMPORTED_MODULE_2__.getProductIdentifier)(productItem));
            const productsToAdd = products.filter(productItem => {
                const productIdentifier = (0,_product_model__WEBPACK_IMPORTED_MODULE_2__.getProductIdentifier)(productItem);
                if (productIdentifier == null || productCollectionIdentifiers.includes(productIdentifier)) {
                    return false;
                }
                productCollectionIdentifiers.push(productIdentifier);
                return true;
            });
            return [...productsToAdd, ...productCollection];
        }
        return productCollection;
    }
}
ProductService.ɵfac = function ProductService_Factory(t) { return new (t || ProductService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
ProductService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: ProductService, factory: ProductService.ɵfac, providedIn: 'root' });


/***/ })

}]);
//# sourceMappingURL=common.js.map