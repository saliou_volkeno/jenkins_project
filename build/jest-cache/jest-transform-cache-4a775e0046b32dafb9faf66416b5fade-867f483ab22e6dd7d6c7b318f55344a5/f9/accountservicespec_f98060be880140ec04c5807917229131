ab0dea46351edffeb3cd7a6b041d402f
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
jest.mock('@angular/router');
jest.mock('@ngx-translate/core');
jest.mock('app/core/auth/state-storage.service');
const router_1 = require("@angular/router");
const testing_1 = require("@angular/common/http/testing");
const testing_2 = require("@angular/core/testing");
const core_1 = require("@ngx-translate/core");
const ngx_webstorage_1 = require("ngx-webstorage");
const authority_constants_1 = require("app/config/authority.constants");
const state_storage_service_1 = require("app/core/auth/state-storage.service");
const application_config_service_1 = require("app/core/config/application-config.service");
const account_service_1 = require("./account.service");
function accountWithAuthorities(authorities) {
    return {
        activated: true,
        authorities,
        email: '',
        firstName: '',
        langKey: '',
        lastName: '',
        login: '',
        imageUrl: '',
    };
}
describe('Service Tests', () => {
    describe('Account Service', () => {
        let service;
        let applicationConfigService;
        let httpMock;
        let mockStorageService;
        let mockRouter;
        let mockTranslateService;
        let sessionStorageService;
        beforeEach(() => {
            testing_2.TestBed.configureTestingModule({
                imports: [testing_1.HttpClientTestingModule, ngx_webstorage_1.NgxWebstorageModule.forRoot()],
                providers: [core_1.TranslateService, state_storage_service_1.StateStorageService, router_1.Router],
            });
            service = testing_2.TestBed.inject(account_service_1.AccountService);
            applicationConfigService = testing_2.TestBed.inject(application_config_service_1.ApplicationConfigService);
            httpMock = testing_2.TestBed.inject(testing_1.HttpTestingController);
            mockStorageService = testing_2.TestBed.inject(state_storage_service_1.StateStorageService);
            mockRouter = testing_2.TestBed.inject(router_1.Router);
            mockTranslateService = testing_2.TestBed.inject(core_1.TranslateService);
            sessionStorageService = testing_2.TestBed.inject(ngx_webstorage_1.SessionStorageService);
        });
        afterEach(() => {
            httpMock.verify();
        });
        describe('save', () => {
            it('should call account saving endpoint with correct values', () => {
                // GIVEN
                const account = accountWithAuthorities([]);
                // WHEN
                service.save(account).subscribe();
                const testRequest = httpMock.expectOne({ method: 'POST', url: applicationConfigService.getEndpointFor('api/account') });
                testRequest.flush({});
                // THEN
                expect(testRequest.request.body).toEqual(account);
            });
        });
        describe('authenticate', () => {
            it('authenticationState should emit null if input is null', () => {
                // GIVEN
                let userIdentity = accountWithAuthorities([]);
                service.getAuthenticationState().subscribe(account => (userIdentity = account));
                // WHEN
                service.authenticate(null);
                // THEN
                expect(userIdentity).toBeNull();
                expect(service.isAuthenticated()).toBe(false);
            });
            it('authenticationState should emit the same account as was in input parameter', () => {
                // GIVEN
                const expectedResult = accountWithAuthorities([]);
                let userIdentity = null;
                service.getAuthenticationState().subscribe(account => (userIdentity = account));
                // WHEN
                service.authenticate(expectedResult);
                // THEN
                expect(userIdentity).toEqual(expectedResult);
                expect(service.isAuthenticated()).toBe(true);
            });
        });
        describe('identity', () => {
            it('should call /account only once if not logged out after first authentication and should call /account again if user has logged out', () => {
                // Given the user is authenticated
                service.identity().subscribe();
                httpMock.expectOne({ method: 'GET' }).flush({});
                // When I call
                service.identity().subscribe();
                // Then there is no second request
                httpMock.expectNone({ method: 'GET' });
                // When I log out
                service.authenticate(null);
                // and then call
                service.identity().subscribe();
                // Then there is a new request
                httpMock.expectOne({ method: 'GET' });
            });
            describe('should change the language on authentication if necessary', () => {
                it('should change language if user has not changed language manually', () => {
                    // GIVEN
                    sessionStorageService.retrieve = jest.fn(key => (key === 'locale' ? undefined : 'otherSessionStorageValue'));
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush(Object.assign(Object.assign({}, accountWithAuthorities([])), { langKey: 'accountLang' }));
                    // THEN
                    expect(mockTranslateService.use).toHaveBeenCalledWith('accountLang');
                });
                it('should not change language if user has changed language manually', () => {
                    // GIVEN
                    sessionStorageService.retrieve = jest.fn(key => (key === 'locale' ? 'sessionLang' : undefined));
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush(Object.assign(Object.assign({}, accountWithAuthorities([])), { langKey: 'accountLang' }));
                    // THEN
                    expect(mockTranslateService.use).not.toHaveBeenCalled();
                });
            });
            describe('navigateToStoredUrl', () => {
                it('should navigate to the previous stored url post successful authentication', () => {
                    // GIVEN
                    mockStorageService.getUrl = jest.fn(() => 'admin/users?page=0');
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush({});
                    // THEN
                    expect(mockStorageService.getUrl).toHaveBeenCalledTimes(1);
                    expect(mockStorageService.clearUrl).toHaveBeenCalledTimes(1);
                    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('admin/users?page=0');
                });
                it('should not navigate to the previous stored url when authentication fails', () => {
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).error(new ErrorEvent(''));
                    // THEN
                    expect(mockStorageService.getUrl).not.toHaveBeenCalled();
                    expect(mockStorageService.clearUrl).not.toHaveBeenCalled();
                    expect(mockRouter.navigateByUrl).not.toHaveBeenCalled();
                });
                it('should not navigate to the previous stored url when no such url exists post successful authentication', () => {
                    // GIVEN
                    mockStorageService.getUrl = jest.fn(() => null);
                    // WHEN
                    service.identity().subscribe();
                    httpMock.expectOne({ method: 'GET' }).flush({});
                    // THEN
                    expect(mockStorageService.getUrl).toHaveBeenCalledTimes(1);
                    expect(mockStorageService.clearUrl).not.toHaveBeenCalled();
                    expect(mockRouter.navigateByUrl).not.toHaveBeenCalled();
                });
            });
        });
        describe('hasAnyAuthority', () => {
            describe('hasAnyAuthority string parameter', () => {
                it('should return false if user is not logged', () => {
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.USER);
                    expect(hasAuthority).toBe(false);
                });
                it('should return false if user is logged and has not authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.ADMIN);
                    expect(hasAuthority).toBe(false);
                });
                it('should return true if user is logged and has authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority(authority_constants_1.Authority.USER);
                    expect(hasAuthority).toBe(true);
                });
            });
            describe('hasAnyAuthority array parameter', () => {
                it('should return false if user is not logged', () => {
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.USER]);
                    expect(hasAuthority).toBeFalsy();
                });
                it('should return false if user is logged and has not authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.ADMIN]);
                    expect(hasAuthority).toBe(false);
                });
                it('should return true if user is logged and has authority', () => {
                    service.authenticate(accountWithAuthorities([authority_constants_1.Authority.USER]));
                    const hasAuthority = service.hasAnyAuthority([authority_constants_1.Authority.USER, authority_constants_1.Authority.ADMIN]);
                    expect(hasAuthority).toBe(true);
                });
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL1VzZXJzL3RlbXAvRGVza3RvcC9KaGlwc3Rlcl9sZWFybm5pbmcvZS1jb21tZXJjZS1hcHAvc3JjL21haW4vd2ViYXBwL2FwcC9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlLnNwZWMudHMiLCJtYXBwaW5ncyI6Ijs7QUFBQSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7QUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMscUNBQXFDLENBQUMsQ0FBQztBQUVqRCw0Q0FBeUM7QUFDekMsMERBQThGO0FBQzlGLG1EQUFnRDtBQUNoRCw4Q0FBdUQ7QUFDdkQsbURBQTRFO0FBRzVFLHdFQUEyRDtBQUMzRCwrRUFBMEU7QUFDMUUsMkZBQXNGO0FBRXRGLHVEQUFtRDtBQUVuRCxTQUFTLHNCQUFzQixDQUFDLFdBQXFCO0lBQ25ELE9BQU87UUFDTCxTQUFTLEVBQUUsSUFBSTtRQUNmLFdBQVc7UUFDWCxLQUFLLEVBQUUsRUFBRTtRQUNULFNBQVMsRUFBRSxFQUFFO1FBQ2IsT0FBTyxFQUFFLEVBQUU7UUFDWCxRQUFRLEVBQUUsRUFBRTtRQUNaLEtBQUssRUFBRSxFQUFFO1FBQ1QsUUFBUSxFQUFFLEVBQUU7S0FDYixDQUFDO0FBQ0osQ0FBQztBQUVELFFBQVEsQ0FBQyxlQUFlLEVBQUUsR0FBRyxFQUFFO0lBQzdCLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7UUFDL0IsSUFBSSxPQUF1QixDQUFDO1FBQzVCLElBQUksd0JBQWtELENBQUM7UUFDdkQsSUFBSSxRQUErQixDQUFDO1FBQ3BDLElBQUksa0JBQXVDLENBQUM7UUFDNUMsSUFBSSxVQUFrQixDQUFDO1FBQ3ZCLElBQUksb0JBQXNDLENBQUM7UUFDM0MsSUFBSSxxQkFBNEMsQ0FBQztRQUVqRCxVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsaUJBQU8sQ0FBQyxzQkFBc0IsQ0FBQztnQkFDN0IsT0FBTyxFQUFFLENBQUMsaUNBQXVCLEVBQUUsb0NBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2pFLFNBQVMsRUFBRSxDQUFDLHVCQUFnQixFQUFFLDJDQUFtQixFQUFFLGVBQU0sQ0FBQzthQUMzRCxDQUFDLENBQUM7WUFFSCxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsZ0NBQWMsQ0FBQyxDQUFDO1lBQ3pDLHdCQUF3QixHQUFHLGlCQUFPLENBQUMsTUFBTSxDQUFDLHFEQUF3QixDQUFDLENBQUM7WUFDcEUsUUFBUSxHQUFHLGlCQUFPLENBQUMsTUFBTSxDQUFDLCtCQUFxQixDQUFDLENBQUM7WUFDakQsa0JBQWtCLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsMkNBQW1CLENBQUMsQ0FBQztZQUN6RCxVQUFVLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsZUFBTSxDQUFDLENBQUM7WUFDcEMsb0JBQW9CLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsdUJBQWdCLENBQUMsQ0FBQztZQUN4RCxxQkFBcUIsR0FBRyxpQkFBTyxDQUFDLE1BQU0sQ0FBQyxzQ0FBcUIsQ0FBQyxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNiLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFO1lBQ3BCLEVBQUUsQ0FBQyx5REFBeUQsRUFBRSxHQUFHLEVBQUU7Z0JBQ2pFLFFBQVE7Z0JBQ1IsTUFBTSxPQUFPLEdBQUcsc0JBQXNCLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRTNDLE9BQU87Z0JBQ1AsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDbEMsTUFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hILFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRXRCLE9BQU87Z0JBQ1AsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsY0FBYyxFQUFFLEdBQUcsRUFBRTtZQUM1QixFQUFFLENBQUMsdURBQXVELEVBQUUsR0FBRyxFQUFFO2dCQUMvRCxRQUFRO2dCQUNSLElBQUksWUFBWSxHQUFtQixzQkFBc0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsT0FBTyxDQUFDLHNCQUFzQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFFaEYsT0FBTztnQkFDUCxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUUzQixPQUFPO2dCQUNQLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDaEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoRCxDQUFDLENBQUMsQ0FBQztZQUVILEVBQUUsQ0FBQyw0RUFBNEUsRUFBRSxHQUFHLEVBQUU7Z0JBQ3BGLFFBQVE7Z0JBQ1IsTUFBTSxjQUFjLEdBQUcsc0JBQXNCLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xELElBQUksWUFBWSxHQUFtQixJQUFJLENBQUM7Z0JBQ3hDLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBRWhGLE9BQU87Z0JBQ1AsT0FBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFFckMsT0FBTztnQkFDUCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUM3QyxNQUFNLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsVUFBVSxFQUFFLEdBQUcsRUFBRTtZQUN4QixFQUFFLENBQUMsbUlBQW1JLEVBQUUsR0FBRyxFQUFFO2dCQUMzSSxrQ0FBa0M7Z0JBQ2xDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFFaEQsY0FBYztnQkFDZCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBRS9CLGtDQUFrQztnQkFDbEMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUV2QyxpQkFBaUI7Z0JBQ2pCLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNCLGdCQUFnQjtnQkFDaEIsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUUvQiw4QkFBOEI7Z0JBQzlCLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztZQUVILFFBQVEsQ0FBQywyREFBMkQsRUFBRSxHQUFHLEVBQUU7Z0JBQ3pFLEVBQUUsQ0FBQyxrRUFBa0UsRUFBRSxHQUFHLEVBQUU7b0JBQzFFLFFBQVE7b0JBQ1IscUJBQXFCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO29CQUU3RyxPQUFPO29CQUNQLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssaUNBQU0sc0JBQXNCLENBQUMsRUFBRSxDQUFDLEtBQUUsT0FBTyxFQUFFLGFBQWEsSUFBRyxDQUFDO29CQUV2RyxPQUFPO29CQUNQLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDdkUsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLGtFQUFrRSxFQUFFLEdBQUcsRUFBRTtvQkFDMUUsUUFBUTtvQkFDUixxQkFBcUIsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUVoRyxPQUFPO29CQUNQLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssaUNBQU0sc0JBQXNCLENBQUMsRUFBRSxDQUFDLEtBQUUsT0FBTyxFQUFFLGFBQWEsSUFBRyxDQUFDO29CQUV2RyxPQUFPO29CQUNQLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUQsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUVILFFBQVEsQ0FBQyxxQkFBcUIsRUFBRSxHQUFHLEVBQUU7Z0JBQ25DLEVBQUUsQ0FBQywyRUFBMkUsRUFBRSxHQUFHLEVBQUU7b0JBQ25GLFFBQVE7b0JBQ1Isa0JBQWtCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFFaEUsT0FBTztvQkFDUCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBRWhELE9BQU87b0JBQ1AsTUFBTSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzdELE1BQU0sQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDOUUsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLDBFQUEwRSxFQUFFLEdBQUcsRUFBRTtvQkFDbEYsT0FBTztvQkFDUCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFFaEUsT0FBTztvQkFDUCxNQUFNLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQ3pELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDM0QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUQsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLHVHQUF1RyxFQUFFLEdBQUcsRUFBRTtvQkFDL0csUUFBUTtvQkFDUixrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFaEQsT0FBTztvQkFDUCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBRWhELE9BQU87b0JBQ1AsTUFBTSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQzNELE1BQU0sQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzFELENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLEVBQUU7WUFDL0IsUUFBUSxDQUFDLGtDQUFrQyxFQUFFLEdBQUcsRUFBRTtnQkFDaEQsRUFBRSxDQUFDLDJDQUEyQyxFQUFFLEdBQUcsRUFBRTtvQkFDbkQsTUFBTSxZQUFZLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM3RCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNuQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxFQUFFLENBQUMsNkRBQTZELEVBQUUsR0FBRyxFQUFFO29CQUNyRSxPQUFPLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRS9ELE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsK0JBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFFOUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLHdEQUF3RCxFQUFFLEdBQUcsRUFBRTtvQkFDaEUsT0FBTyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUUvRCxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRTdELE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxRQUFRLENBQUMsaUNBQWlDLEVBQUUsR0FBRyxFQUFFO2dCQUMvQyxFQUFFLENBQUMsMkNBQTJDLEVBQUUsR0FBRyxFQUFFO29CQUNuRCxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUMvRCxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ25DLENBQUMsQ0FBQyxDQUFDO2dCQUVILEVBQUUsQ0FBQyw2REFBNkQsRUFBRSxHQUFHLEVBQUU7b0JBQ3JFLE9BQU8sQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQywrQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFL0QsTUFBTSxZQUFZLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDLCtCQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFFaEUsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsRUFBRSxDQUFDLHdEQUF3RCxFQUFFLEdBQUcsRUFBRTtvQkFDaEUsT0FBTyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLCtCQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUUvRCxNQUFNLFlBQVksR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsK0JBQVMsQ0FBQyxJQUFJLEVBQUUsK0JBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUVoRixNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQyxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyxDQUFDIiwibmFtZXMiOltdLCJzb3VyY2VzIjpbIi9Vc2Vycy90ZW1wL0Rlc2t0b3AvSmhpcHN0ZXJfbGVhcm5uaW5nL2UtY29tbWVyY2UtYXBwL3NyYy9tYWluL3dlYmFwcC9hcHAvY29yZS9hdXRoL2FjY291bnQuc2VydmljZS5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImplc3QubW9jaygnQGFuZ3VsYXIvcm91dGVyJyk7XG5qZXN0Lm1vY2soJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnKTtcbmplc3QubW9jaygnYXBwL2NvcmUvYXV0aC9zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnKTtcblxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEh0dHBDbGllbnRUZXN0aW5nTW9kdWxlLCBIdHRwVGVzdGluZ0NvbnRyb2xsZXIgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cC90ZXN0aW5nJztcbmltcG9ydCB7IFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgTmd4V2Vic3RvcmFnZU1vZHVsZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXdlYnN0b3JhZ2UnO1xuXG5pbXBvcnQgeyBBY2NvdW50IH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9hY2NvdW50Lm1vZGVsJztcbmltcG9ydCB7IEF1dGhvcml0eSB9IGZyb20gJ2FwcC9jb25maWcvYXV0aG9yaXR5LmNvbnN0YW50cyc7XG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvYXV0aC9zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdTZXJ2aWNlIH0gZnJvbSAnYXBwL2NvcmUvY29uZmlnL2FwcGxpY2F0aW9uLWNvbmZpZy5zZXJ2aWNlJztcblxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICcuL2FjY291bnQuc2VydmljZSc7XG5cbmZ1bmN0aW9uIGFjY291bnRXaXRoQXV0aG9yaXRpZXMoYXV0aG9yaXRpZXM6IHN0cmluZ1tdKTogQWNjb3VudCB7XG4gIHJldHVybiB7XG4gICAgYWN0aXZhdGVkOiB0cnVlLFxuICAgIGF1dGhvcml0aWVzLFxuICAgIGVtYWlsOiAnJyxcbiAgICBmaXJzdE5hbWU6ICcnLFxuICAgIGxhbmdLZXk6ICcnLFxuICAgIGxhc3ROYW1lOiAnJyxcbiAgICBsb2dpbjogJycsXG4gICAgaW1hZ2VVcmw6ICcnLFxuICB9O1xufVxuXG5kZXNjcmliZSgnU2VydmljZSBUZXN0cycsICgpID0+IHtcbiAgZGVzY3JpYmUoJ0FjY291bnQgU2VydmljZScsICgpID0+IHtcbiAgICBsZXQgc2VydmljZTogQWNjb3VudFNlcnZpY2U7XG4gICAgbGV0IGFwcGxpY2F0aW9uQ29uZmlnU2VydmljZTogQXBwbGljYXRpb25Db25maWdTZXJ2aWNlO1xuICAgIGxldCBodHRwTW9jazogSHR0cFRlc3RpbmdDb250cm9sbGVyO1xuICAgIGxldCBtb2NrU3RvcmFnZVNlcnZpY2U6IFN0YXRlU3RvcmFnZVNlcnZpY2U7XG4gICAgbGV0IG1vY2tSb3V0ZXI6IFJvdXRlcjtcbiAgICBsZXQgbW9ja1RyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2U7XG4gICAgbGV0IHNlc3Npb25TdG9yYWdlU2VydmljZTogU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbSHR0cENsaWVudFRlc3RpbmdNb2R1bGUsIE5neFdlYnN0b3JhZ2VNb2R1bGUuZm9yUm9vdCgpXSxcbiAgICAgICAgcHJvdmlkZXJzOiBbVHJhbnNsYXRlU2VydmljZSwgU3RhdGVTdG9yYWdlU2VydmljZSwgUm91dGVyXSxcbiAgICAgIH0pO1xuXG4gICAgICBzZXJ2aWNlID0gVGVzdEJlZC5pbmplY3QoQWNjb3VudFNlcnZpY2UpO1xuICAgICAgYXBwbGljYXRpb25Db25maWdTZXJ2aWNlID0gVGVzdEJlZC5pbmplY3QoQXBwbGljYXRpb25Db25maWdTZXJ2aWNlKTtcbiAgICAgIGh0dHBNb2NrID0gVGVzdEJlZC5pbmplY3QoSHR0cFRlc3RpbmdDb250cm9sbGVyKTtcbiAgICAgIG1vY2tTdG9yYWdlU2VydmljZSA9IFRlc3RCZWQuaW5qZWN0KFN0YXRlU3RvcmFnZVNlcnZpY2UpO1xuICAgICAgbW9ja1JvdXRlciA9IFRlc3RCZWQuaW5qZWN0KFJvdXRlcik7XG4gICAgICBtb2NrVHJhbnNsYXRlU2VydmljZSA9IFRlc3RCZWQuaW5qZWN0KFRyYW5zbGF0ZVNlcnZpY2UpO1xuICAgICAgc2Vzc2lvblN0b3JhZ2VTZXJ2aWNlID0gVGVzdEJlZC5pbmplY3QoU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKTtcbiAgICB9KTtcblxuICAgIGFmdGVyRWFjaCgoKSA9PiB7XG4gICAgICBodHRwTW9jay52ZXJpZnkoKTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdzYXZlJywgKCkgPT4ge1xuICAgICAgaXQoJ3Nob3VsZCBjYWxsIGFjY291bnQgc2F2aW5nIGVuZHBvaW50IHdpdGggY29ycmVjdCB2YWx1ZXMnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGFjY291bnQgPSBhY2NvdW50V2l0aEF1dGhvcml0aWVzKFtdKTtcblxuICAgICAgICAvLyBXSEVOXG4gICAgICAgIHNlcnZpY2Uuc2F2ZShhY2NvdW50KS5zdWJzY3JpYmUoKTtcbiAgICAgICAgY29uc3QgdGVzdFJlcXVlc3QgPSBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdQT1NUJywgdXJsOiBhcHBsaWNhdGlvbkNvbmZpZ1NlcnZpY2UuZ2V0RW5kcG9pbnRGb3IoJ2FwaS9hY2NvdW50JykgfSk7XG4gICAgICAgIHRlc3RSZXF1ZXN0LmZsdXNoKHt9KTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdCh0ZXN0UmVxdWVzdC5yZXF1ZXN0LmJvZHkpLnRvRXF1YWwoYWNjb3VudCk7XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdhdXRoZW50aWNhdGUnLCAoKSA9PiB7XG4gICAgICBpdCgnYXV0aGVudGljYXRpb25TdGF0ZSBzaG91bGQgZW1pdCBudWxsIGlmIGlucHV0IGlzIG51bGwnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGxldCB1c2VySWRlbnRpdHk6IEFjY291bnQgfCBudWxsID0gYWNjb3VudFdpdGhBdXRob3JpdGllcyhbXSk7XG4gICAgICAgIHNlcnZpY2UuZ2V0QXV0aGVudGljYXRpb25TdGF0ZSgpLnN1YnNjcmliZShhY2NvdW50ID0+ICh1c2VySWRlbnRpdHkgPSBhY2NvdW50KSk7XG5cbiAgICAgICAgLy8gV0hFTlxuICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShudWxsKTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdCh1c2VySWRlbnRpdHkpLnRvQmVOdWxsKCk7XG4gICAgICAgIGV4cGVjdChzZXJ2aWNlLmlzQXV0aGVudGljYXRlZCgpKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pO1xuXG4gICAgICBpdCgnYXV0aGVudGljYXRpb25TdGF0ZSBzaG91bGQgZW1pdCB0aGUgc2FtZSBhY2NvdW50IGFzIHdhcyBpbiBpbnB1dCBwYXJhbWV0ZXInLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIGNvbnN0IGV4cGVjdGVkUmVzdWx0ID0gYWNjb3VudFdpdGhBdXRob3JpdGllcyhbXSk7XG4gICAgICAgIGxldCB1c2VySWRlbnRpdHk6IEFjY291bnQgfCBudWxsID0gbnVsbDtcbiAgICAgICAgc2VydmljZS5nZXRBdXRoZW50aWNhdGlvblN0YXRlKCkuc3Vic2NyaWJlKGFjY291bnQgPT4gKHVzZXJJZGVudGl0eSA9IGFjY291bnQpKTtcblxuICAgICAgICAvLyBXSEVOXG4gICAgICAgIHNlcnZpY2UuYXV0aGVudGljYXRlKGV4cGVjdGVkUmVzdWx0KTtcblxuICAgICAgICAvLyBUSEVOXG4gICAgICAgIGV4cGVjdCh1c2VySWRlbnRpdHkpLnRvRXF1YWwoZXhwZWN0ZWRSZXN1bHQpO1xuICAgICAgICBleHBlY3Qoc2VydmljZS5pc0F1dGhlbnRpY2F0ZWQoKSkudG9CZSh0cnVlKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ2lkZW50aXR5JywgKCkgPT4ge1xuICAgICAgaXQoJ3Nob3VsZCBjYWxsIC9hY2NvdW50IG9ubHkgb25jZSBpZiBub3QgbG9nZ2VkIG91dCBhZnRlciBmaXJzdCBhdXRoZW50aWNhdGlvbiBhbmQgc2hvdWxkIGNhbGwgL2FjY291bnQgYWdhaW4gaWYgdXNlciBoYXMgbG9nZ2VkIG91dCcsICgpID0+IHtcbiAgICAgICAgLy8gR2l2ZW4gdGhlIHVzZXIgaXMgYXV0aGVudGljYXRlZFxuICAgICAgICBzZXJ2aWNlLmlkZW50aXR5KCkuc3Vic2NyaWJlKCk7XG4gICAgICAgIGh0dHBNb2NrLmV4cGVjdE9uZSh7IG1ldGhvZDogJ0dFVCcgfSkuZmx1c2goe30pO1xuXG4gICAgICAgIC8vIFdoZW4gSSBjYWxsXG4gICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcblxuICAgICAgICAvLyBUaGVuIHRoZXJlIGlzIG5vIHNlY29uZCByZXF1ZXN0XG4gICAgICAgIGh0dHBNb2NrLmV4cGVjdE5vbmUoeyBtZXRob2Q6ICdHRVQnIH0pO1xuXG4gICAgICAgIC8vIFdoZW4gSSBsb2cgb3V0XG4gICAgICAgIHNlcnZpY2UuYXV0aGVudGljYXRlKG51bGwpO1xuICAgICAgICAvLyBhbmQgdGhlbiBjYWxsXG4gICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcblxuICAgICAgICAvLyBUaGVuIHRoZXJlIGlzIGEgbmV3IHJlcXVlc3RcbiAgICAgICAgaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KTtcbiAgICAgIH0pO1xuXG4gICAgICBkZXNjcmliZSgnc2hvdWxkIGNoYW5nZSB0aGUgbGFuZ3VhZ2Ugb24gYXV0aGVudGljYXRpb24gaWYgbmVjZXNzYXJ5JywgKCkgPT4ge1xuICAgICAgICBpdCgnc2hvdWxkIGNoYW5nZSBsYW5ndWFnZSBpZiB1c2VyIGhhcyBub3QgY2hhbmdlZCBsYW5ndWFnZSBtYW51YWxseScsICgpID0+IHtcbiAgICAgICAgICAvLyBHSVZFTlxuICAgICAgICAgIHNlc3Npb25TdG9yYWdlU2VydmljZS5yZXRyaWV2ZSA9IGplc3QuZm4oa2V5ID0+IChrZXkgPT09ICdsb2NhbGUnID8gdW5kZWZpbmVkIDogJ290aGVyU2Vzc2lvblN0b3JhZ2VWYWx1ZScpKTtcblxuICAgICAgICAgIC8vIFdIRU5cbiAgICAgICAgICBzZXJ2aWNlLmlkZW50aXR5KCkuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KS5mbHVzaCh7IC4uLmFjY291bnRXaXRoQXV0aG9yaXRpZXMoW10pLCBsYW5nS2V5OiAnYWNjb3VudExhbmcnIH0pO1xuXG4gICAgICAgICAgLy8gVEhFTlxuICAgICAgICAgIGV4cGVjdChtb2NrVHJhbnNsYXRlU2VydmljZS51c2UpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKCdhY2NvdW50TGFuZycpO1xuICAgICAgICB9KTtcblxuICAgICAgICBpdCgnc2hvdWxkIG5vdCBjaGFuZ2UgbGFuZ3VhZ2UgaWYgdXNlciBoYXMgY2hhbmdlZCBsYW5ndWFnZSBtYW51YWxseScsICgpID0+IHtcbiAgICAgICAgICAvLyBHSVZFTlxuICAgICAgICAgIHNlc3Npb25TdG9yYWdlU2VydmljZS5yZXRyaWV2ZSA9IGplc3QuZm4oa2V5ID0+IChrZXkgPT09ICdsb2NhbGUnID8gJ3Nlc3Npb25MYW5nJyA6IHVuZGVmaW5lZCkpO1xuXG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcbiAgICAgICAgICBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdHRVQnIH0pLmZsdXNoKHsgLi4uYWNjb3VudFdpdGhBdXRob3JpdGllcyhbXSksIGxhbmdLZXk6ICdhY2NvdW50TGFuZycgfSk7XG5cbiAgICAgICAgICAvLyBUSEVOXG4gICAgICAgICAgZXhwZWN0KG1vY2tUcmFuc2xhdGVTZXJ2aWNlLnVzZSkubm90LnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcblxuICAgICAgZGVzY3JpYmUoJ25hdmlnYXRlVG9TdG9yZWRVcmwnLCAoKSA9PiB7XG4gICAgICAgIGl0KCdzaG91bGQgbmF2aWdhdGUgdG8gdGhlIHByZXZpb3VzIHN0b3JlZCB1cmwgcG9zdCBzdWNjZXNzZnVsIGF1dGhlbnRpY2F0aW9uJywgKCkgPT4ge1xuICAgICAgICAgIC8vIEdJVkVOXG4gICAgICAgICAgbW9ja1N0b3JhZ2VTZXJ2aWNlLmdldFVybCA9IGplc3QuZm4oKCkgPT4gJ2FkbWluL3VzZXJzP3BhZ2U9MCcpO1xuXG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcbiAgICAgICAgICBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdHRVQnIH0pLmZsdXNoKHt9KTtcblxuICAgICAgICAgIC8vIFRIRU5cbiAgICAgICAgICBleHBlY3QobW9ja1N0b3JhZ2VTZXJ2aWNlLmdldFVybCkudG9IYXZlQmVlbkNhbGxlZFRpbWVzKDEpO1xuICAgICAgICAgIGV4cGVjdChtb2NrU3RvcmFnZVNlcnZpY2UuY2xlYXJVcmwpLnRvSGF2ZUJlZW5DYWxsZWRUaW1lcygxKTtcbiAgICAgICAgICBleHBlY3QobW9ja1JvdXRlci5uYXZpZ2F0ZUJ5VXJsKS50b0hhdmVCZWVuQ2FsbGVkV2l0aCgnYWRtaW4vdXNlcnM/cGFnZT0wJyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGl0KCdzaG91bGQgbm90IG5hdmlnYXRlIHRvIHRoZSBwcmV2aW91cyBzdG9yZWQgdXJsIHdoZW4gYXV0aGVudGljYXRpb24gZmFpbHMnLCAoKSA9PiB7XG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIHNlcnZpY2UuaWRlbnRpdHkoKS5zdWJzY3JpYmUoKTtcbiAgICAgICAgICBodHRwTW9jay5leHBlY3RPbmUoeyBtZXRob2Q6ICdHRVQnIH0pLmVycm9yKG5ldyBFcnJvckV2ZW50KCcnKSk7XG5cbiAgICAgICAgICAvLyBUSEVOXG4gICAgICAgICAgZXhwZWN0KG1vY2tTdG9yYWdlU2VydmljZS5nZXRVcmwpLm5vdC50b0hhdmVCZWVuQ2FsbGVkKCk7XG4gICAgICAgICAgZXhwZWN0KG1vY2tTdG9yYWdlU2VydmljZS5jbGVhclVybCkubm90LnRvSGF2ZUJlZW5DYWxsZWQoKTtcbiAgICAgICAgICBleHBlY3QobW9ja1JvdXRlci5uYXZpZ2F0ZUJ5VXJsKS5ub3QudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICB9KTtcblxuICAgICAgICBpdCgnc2hvdWxkIG5vdCBuYXZpZ2F0ZSB0byB0aGUgcHJldmlvdXMgc3RvcmVkIHVybCB3aGVuIG5vIHN1Y2ggdXJsIGV4aXN0cyBwb3N0IHN1Y2Nlc3NmdWwgYXV0aGVudGljYXRpb24nLCAoKSA9PiB7XG4gICAgICAgICAgLy8gR0lWRU5cbiAgICAgICAgICBtb2NrU3RvcmFnZVNlcnZpY2UuZ2V0VXJsID0gamVzdC5mbigoKSA9PiBudWxsKTtcblxuICAgICAgICAgIC8vIFdIRU5cbiAgICAgICAgICBzZXJ2aWNlLmlkZW50aXR5KCkuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgaHR0cE1vY2suZXhwZWN0T25lKHsgbWV0aG9kOiAnR0VUJyB9KS5mbHVzaCh7fSk7XG5cbiAgICAgICAgICAvLyBUSEVOXG4gICAgICAgICAgZXhwZWN0KG1vY2tTdG9yYWdlU2VydmljZS5nZXRVcmwpLnRvSGF2ZUJlZW5DYWxsZWRUaW1lcygxKTtcbiAgICAgICAgICBleHBlY3QobW9ja1N0b3JhZ2VTZXJ2aWNlLmNsZWFyVXJsKS5ub3QudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICAgIGV4cGVjdChtb2NrUm91dGVyLm5hdmlnYXRlQnlVcmwpLm5vdC50b0hhdmVCZWVuQ2FsbGVkKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnaGFzQW55QXV0aG9yaXR5JywgKCkgPT4ge1xuICAgICAgZGVzY3JpYmUoJ2hhc0FueUF1dGhvcml0eSBzdHJpbmcgcGFyYW1ldGVyJywgKCkgPT4ge1xuICAgICAgICBpdCgnc2hvdWxkIHJldHVybiBmYWxzZSBpZiB1c2VyIGlzIG5vdCBsb2dnZWQnLCAoKSA9PiB7XG4gICAgICAgICAgY29uc3QgaGFzQXV0aG9yaXR5ID0gc2VydmljZS5oYXNBbnlBdXRob3JpdHkoQXV0aG9yaXR5LlVTRVIpO1xuICAgICAgICAgIGV4cGVjdChoYXNBdXRob3JpdHkpLnRvQmUoZmFsc2UpO1xuICAgICAgICB9KTtcblxuICAgICAgICBpdCgnc2hvdWxkIHJldHVybiBmYWxzZSBpZiB1c2VyIGlzIGxvZ2dlZCBhbmQgaGFzIG5vdCBhdXRob3JpdHknLCAoKSA9PiB7XG4gICAgICAgICAgc2VydmljZS5hdXRoZW50aWNhdGUoYWNjb3VudFdpdGhBdXRob3JpdGllcyhbQXV0aG9yaXR5LlVTRVJdKSk7XG5cbiAgICAgICAgICBjb25zdCBoYXNBdXRob3JpdHkgPSBzZXJ2aWNlLmhhc0FueUF1dGhvcml0eShBdXRob3JpdHkuQURNSU4pO1xuXG4gICAgICAgICAgZXhwZWN0KGhhc0F1dGhvcml0eSkudG9CZShmYWxzZSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGl0KCdzaG91bGQgcmV0dXJuIHRydWUgaWYgdXNlciBpcyBsb2dnZWQgYW5kIGhhcyBhdXRob3JpdHknLCAoKSA9PiB7XG4gICAgICAgICAgc2VydmljZS5hdXRoZW50aWNhdGUoYWNjb3VudFdpdGhBdXRob3JpdGllcyhbQXV0aG9yaXR5LlVTRVJdKSk7XG5cbiAgICAgICAgICBjb25zdCBoYXNBdXRob3JpdHkgPSBzZXJ2aWNlLmhhc0FueUF1dGhvcml0eShBdXRob3JpdHkuVVNFUik7XG5cbiAgICAgICAgICBleHBlY3QoaGFzQXV0aG9yaXR5KS50b0JlKHRydWUpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuXG4gICAgICBkZXNjcmliZSgnaGFzQW55QXV0aG9yaXR5IGFycmF5IHBhcmFtZXRlcicsICgpID0+IHtcbiAgICAgICAgaXQoJ3Nob3VsZCByZXR1cm4gZmFsc2UgaWYgdXNlciBpcyBub3QgbG9nZ2VkJywgKCkgPT4ge1xuICAgICAgICAgIGNvbnN0IGhhc0F1dGhvcml0eSA9IHNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KFtBdXRob3JpdHkuVVNFUl0pO1xuICAgICAgICAgIGV4cGVjdChoYXNBdXRob3JpdHkpLnRvQmVGYWxzeSgpO1xuICAgICAgICB9KTtcblxuICAgICAgICBpdCgnc2hvdWxkIHJldHVybiBmYWxzZSBpZiB1c2VyIGlzIGxvZ2dlZCBhbmQgaGFzIG5vdCBhdXRob3JpdHknLCAoKSA9PiB7XG4gICAgICAgICAgc2VydmljZS5hdXRoZW50aWNhdGUoYWNjb3VudFdpdGhBdXRob3JpdGllcyhbQXV0aG9yaXR5LlVTRVJdKSk7XG5cbiAgICAgICAgICBjb25zdCBoYXNBdXRob3JpdHkgPSBzZXJ2aWNlLmhhc0FueUF1dGhvcml0eShbQXV0aG9yaXR5LkFETUlOXSk7XG5cbiAgICAgICAgICBleHBlY3QoaGFzQXV0aG9yaXR5KS50b0JlKGZhbHNlKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaXQoJ3Nob3VsZCByZXR1cm4gdHJ1ZSBpZiB1c2VyIGlzIGxvZ2dlZCBhbmQgaGFzIGF1dGhvcml0eScsICgpID0+IHtcbiAgICAgICAgICBzZXJ2aWNlLmF1dGhlbnRpY2F0ZShhY2NvdW50V2l0aEF1dGhvcml0aWVzKFtBdXRob3JpdHkuVVNFUl0pKTtcblxuICAgICAgICAgIGNvbnN0IGhhc0F1dGhvcml0eSA9IHNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KFtBdXRob3JpdHkuVVNFUiwgQXV0aG9yaXR5LkFETUlOXSk7XG5cbiAgICAgICAgICBleHBlY3QoaGFzQXV0aG9yaXR5KS50b0JlKHRydWUpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9