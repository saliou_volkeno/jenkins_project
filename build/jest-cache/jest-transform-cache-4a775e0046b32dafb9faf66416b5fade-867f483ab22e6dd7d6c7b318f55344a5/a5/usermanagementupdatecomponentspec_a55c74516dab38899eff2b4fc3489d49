197d3ff80dcc9125e6aae647a341fa43
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testing_1 = require("@angular/core/testing");
const testing_2 = require("@angular/common/http/testing");
const forms_1 = require("@angular/forms");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const authority_constants_1 = require("app/config/authority.constants");
const user_management_service_1 = require("../service/user-management.service");
const user_management_model_1 = require("../user-management.model");
const user_management_update_component_1 = require("./user-management-update.component");
describe('Component Tests', () => {
    describe('User Management Update Component', () => {
        let comp;
        let fixture;
        let service;
        beforeEach(testing_1.waitForAsync(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [testing_2.HttpClientTestingModule],
                declarations: [user_management_update_component_1.UserManagementUpdateComponent],
                providers: [
                    forms_1.FormBuilder,
                    {
                        provide: router_1.ActivatedRoute,
                        useValue: {
                            data: rxjs_1.of({ user: new user_management_model_1.User(123, 'user', 'first', 'last', 'first@last.com', true, 'en', [authority_constants_1.Authority.USER], 'admin') }),
                        },
                    },
                ],
            })
                .overrideTemplate(user_management_update_component_1.UserManagementUpdateComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(user_management_update_component_1.UserManagementUpdateComponent);
            comp = fixture.componentInstance;
            service = testing_1.TestBed.inject(user_management_service_1.UserManagementService);
        });
        describe('OnInit', () => {
            it('Should load authorities and language on init', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                jest.spyOn(service, 'authorities').mockReturnValue(rxjs_1.of(['USER']));
                // WHEN
                comp.ngOnInit();
                // THEN
                expect(service.authorities).toHaveBeenCalled();
                expect(comp.authorities).toEqual(['USER']);
            })));
        });
        describe('save', () => {
            it('Should call update service on save for existing user', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                const entity = new user_management_model_1.User(123);
                jest.spyOn(service, 'update').mockReturnValue(rxjs_1.of(entity));
                comp.user = entity;
                comp.editForm.patchValue({ id: entity.id });
                // WHEN
                comp.save();
                testing_1.tick(); // simulate async
                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            })));
            it('Should call create service on save for new user', testing_1.inject([], testing_1.fakeAsync(() => {
                // GIVEN
                const entity = new user_management_model_1.User();
                jest.spyOn(service, 'create').mockReturnValue(rxjs_1.of(entity));
                comp.user = entity;
                // WHEN
                comp.save();
                testing_1.tick(); // simulate async
                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            })));
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL1VzZXJzL3RlbXAvRGVza3RvcC9KaGlwc3Rlcl9sZWFybm5pbmcvZS1jb21tZXJjZS1hcHAvc3JjL21haW4vd2ViYXBwL2FwcC9hZG1pbi91c2VyLW1hbmFnZW1lbnQvdXBkYXRlL3VzZXItbWFuYWdlbWVudC11cGRhdGUuY29tcG9uZW50LnNwZWMudHMiLCJtYXBwaW5ncyI6Ijs7QUFBQSxtREFBeUc7QUFDekcsMERBQXVFO0FBQ3ZFLDBDQUE2QztBQUM3Qyw0Q0FBaUQ7QUFDakQsK0JBQTBCO0FBRTFCLHdFQUEyRDtBQUMzRCxnRkFBMkU7QUFDM0Usb0VBQWdEO0FBRWhELHlGQUFtRjtBQUVuRixRQUFRLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxFQUFFO0lBQy9CLFFBQVEsQ0FBQyxrQ0FBa0MsRUFBRSxHQUFHLEVBQUU7UUFDaEQsSUFBSSxJQUFtQyxDQUFDO1FBQ3hDLElBQUksT0FBd0QsQ0FBQztRQUM3RCxJQUFJLE9BQThCLENBQUM7UUFFbkMsVUFBVSxDQUNSLHNCQUFZLENBQUMsR0FBRyxFQUFFO1lBQ2hCLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUF1QixDQUFDO2dCQUNsQyxZQUFZLEVBQUUsQ0FBQyxnRUFBNkIsQ0FBQztnQkFDN0MsU0FBUyxFQUFFO29CQUNULG1CQUFXO29CQUNYO3dCQUNFLE9BQU8sRUFBRSx1QkFBYzt3QkFDdkIsUUFBUSxFQUFFOzRCQUNSLElBQUksRUFBRSxTQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSw0QkFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsK0JBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRSxDQUFDO3lCQUNwSDtxQkFDRjtpQkFDRjthQUNGLENBQUM7aUJBQ0MsZ0JBQWdCLENBQUMsZ0VBQTZCLEVBQUUsRUFBRSxDQUFDO2lCQUNuRCxpQkFBaUIsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUM7UUFFRixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLGdFQUE2QixDQUFDLENBQUM7WUFDakUsSUFBSSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztZQUNqQyxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsK0NBQXFCLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO1lBQ3RCLEVBQUUsQ0FBQyw4Q0FBOEMsRUFBRSxnQkFBTSxDQUN2RCxFQUFFLEVBQ0YsbUJBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2IsUUFBUTtnQkFDUixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQyxlQUFlLENBQUMsU0FBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUVqRSxPQUFPO2dCQUNQLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFFaEIsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQy9DLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUM3QyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFO1lBQ3BCLEVBQUUsQ0FBQyxzREFBc0QsRUFBRSxnQkFBTSxDQUMvRCxFQUFFLEVBQ0YsbUJBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2IsUUFBUTtnQkFDUixNQUFNLE1BQU0sR0FBRyxJQUFJLDRCQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUM1QyxPQUFPO2dCQUNQLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDWixjQUFJLEVBQUUsQ0FBQyxDQUFDLGlCQUFpQjtnQkFFekIsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwRCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsaURBQWlELEVBQUUsZ0JBQU0sQ0FDMUQsRUFBRSxFQUNGLG1CQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNiLFFBQVE7Z0JBQ1IsTUFBTSxNQUFNLEdBQUcsSUFBSSw0QkFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Z0JBQ25CLE9BQU87Z0JBQ1AsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNaLGNBQUksRUFBRSxDQUFDLENBQUMsaUJBQWlCO2dCQUV6QixPQUFPO2dCQUNQLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3BELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxDQUNILENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvVXNlcnMvdGVtcC9EZXNrdG9wL0poaXBzdGVyX2xlYXJubmluZy9lLWNvbW1lcmNlLWFwcC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FkbWluL3VzZXItbWFuYWdlbWVudC91cGRhdGUvdXNlci1tYW5hZ2VtZW50LXVwZGF0ZS5jb21wb25lbnQuc3BlYy50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRGaXh0dXJlLCBUZXN0QmVkLCB3YWl0Rm9yQXN5bmMsIGluamVjdCwgZmFrZUFzeW5jLCB0aWNrIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcbmltcG9ydCB7IEh0dHBDbGllbnRUZXN0aW5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAvdGVzdGluZyc7XG5pbXBvcnQgeyBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IEF1dGhvcml0eSB9IGZyb20gJ2FwcC9jb25maWcvYXV0aG9yaXR5LmNvbnN0YW50cyc7XG5pbXBvcnQgeyBVc2VyTWFuYWdlbWVudFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL3VzZXItbWFuYWdlbWVudC5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuLi91c2VyLW1hbmFnZW1lbnQubW9kZWwnO1xuXG5pbXBvcnQgeyBVc2VyTWFuYWdlbWVudFVwZGF0ZUNvbXBvbmVudCB9IGZyb20gJy4vdXNlci1tYW5hZ2VtZW50LXVwZGF0ZS5jb21wb25lbnQnO1xuXG5kZXNjcmliZSgnQ29tcG9uZW50IFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnVXNlciBNYW5hZ2VtZW50IFVwZGF0ZSBDb21wb25lbnQnLCAoKSA9PiB7XG4gICAgbGV0IGNvbXA6IFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50O1xuICAgIGxldCBmaXh0dXJlOiBDb21wb25lbnRGaXh0dXJlPFVzZXJNYW5hZ2VtZW50VXBkYXRlQ29tcG9uZW50PjtcbiAgICBsZXQgc2VydmljZTogVXNlck1hbmFnZW1lbnRTZXJ2aWNlO1xuXG4gICAgYmVmb3JlRWFjaChcbiAgICAgIHdhaXRGb3JBc3luYygoKSA9PiB7XG4gICAgICAgIFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgICAgaW1wb3J0czogW0h0dHBDbGllbnRUZXN0aW5nTW9kdWxlXSxcbiAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtVc2VyTWFuYWdlbWVudFVwZGF0ZUNvbXBvbmVudF0sXG4gICAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgICBGb3JtQnVpbGRlcixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgcHJvdmlkZTogQWN0aXZhdGVkUm91dGUsXG4gICAgICAgICAgICAgIHVzZVZhbHVlOiB7XG4gICAgICAgICAgICAgICAgZGF0YTogb2YoeyB1c2VyOiBuZXcgVXNlcigxMjMsICd1c2VyJywgJ2ZpcnN0JywgJ2xhc3QnLCAnZmlyc3RAbGFzdC5jb20nLCB0cnVlLCAnZW4nLCBbQXV0aG9yaXR5LlVTRVJdLCAnYWRtaW4nKSB9KSxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgXSxcbiAgICAgICAgfSlcbiAgICAgICAgICAub3ZlcnJpZGVUZW1wbGF0ZShVc2VyTWFuYWdlbWVudFVwZGF0ZUNvbXBvbmVudCwgJycpXG4gICAgICAgICAgLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgICB9KVxuICAgICk7XG5cbiAgICBiZWZvcmVFYWNoKCgpID0+IHtcbiAgICAgIGZpeHR1cmUgPSBUZXN0QmVkLmNyZWF0ZUNvbXBvbmVudChVc2VyTWFuYWdlbWVudFVwZGF0ZUNvbXBvbmVudCk7XG4gICAgICBjb21wID0gZml4dHVyZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICAgIHNlcnZpY2UgPSBUZXN0QmVkLmluamVjdChVc2VyTWFuYWdlbWVudFNlcnZpY2UpO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ09uSW5pdCcsICgpID0+IHtcbiAgICAgIGl0KCdTaG91bGQgbG9hZCBhdXRob3JpdGllcyBhbmQgbGFuZ3VhZ2Ugb24gaW5pdCcsIGluamVjdChcbiAgICAgICAgW10sXG4gICAgICAgIGZha2VBc3luYygoKSA9PiB7XG4gICAgICAgICAgLy8gR0lWRU5cbiAgICAgICAgICBqZXN0LnNweU9uKHNlcnZpY2UsICdhdXRob3JpdGllcycpLm1vY2tSZXR1cm5WYWx1ZShvZihbJ1VTRVInXSkpO1xuXG4gICAgICAgICAgLy8gV0hFTlxuICAgICAgICAgIGNvbXAubmdPbkluaXQoKTtcblxuICAgICAgICAgIC8vIFRIRU5cbiAgICAgICAgICBleHBlY3Qoc2VydmljZS5hdXRob3JpdGllcykudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICAgIGV4cGVjdChjb21wLmF1dGhvcml0aWVzKS50b0VxdWFsKFsnVVNFUiddKTtcbiAgICAgICAgfSlcbiAgICAgICkpO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ3NhdmUnLCAoKSA9PiB7XG4gICAgICBpdCgnU2hvdWxkIGNhbGwgdXBkYXRlIHNlcnZpY2Ugb24gc2F2ZSBmb3IgZXhpc3RpbmcgdXNlcicsIGluamVjdChcbiAgICAgICAgW10sXG4gICAgICAgIGZha2VBc3luYygoKSA9PiB7XG4gICAgICAgICAgLy8gR0lWRU5cbiAgICAgICAgICBjb25zdCBlbnRpdHkgPSBuZXcgVXNlcigxMjMpO1xuICAgICAgICAgIGplc3Quc3B5T24oc2VydmljZSwgJ3VwZGF0ZScpLm1vY2tSZXR1cm5WYWx1ZShvZihlbnRpdHkpKTtcbiAgICAgICAgICBjb21wLnVzZXIgPSBlbnRpdHk7XG4gICAgICAgICAgY29tcC5lZGl0Rm9ybS5wYXRjaFZhbHVlKHsgaWQ6IGVudGl0eS5pZCB9KTtcbiAgICAgICAgICAvLyBXSEVOXG4gICAgICAgICAgY29tcC5zYXZlKCk7XG4gICAgICAgICAgdGljaygpOyAvLyBzaW11bGF0ZSBhc3luY1xuXG4gICAgICAgICAgLy8gVEhFTlxuICAgICAgICAgIGV4cGVjdChzZXJ2aWNlLnVwZGF0ZSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoZW50aXR5KTtcbiAgICAgICAgICBleHBlY3QoY29tcC5pc1NhdmluZykudG9FcXVhbChmYWxzZSk7XG4gICAgICAgIH0pXG4gICAgICApKTtcblxuICAgICAgaXQoJ1Nob3VsZCBjYWxsIGNyZWF0ZSBzZXJ2aWNlIG9uIHNhdmUgZm9yIG5ldyB1c2VyJywgaW5qZWN0KFxuICAgICAgICBbXSxcbiAgICAgICAgZmFrZUFzeW5jKCgpID0+IHtcbiAgICAgICAgICAvLyBHSVZFTlxuICAgICAgICAgIGNvbnN0IGVudGl0eSA9IG5ldyBVc2VyKCk7XG4gICAgICAgICAgamVzdC5zcHlPbihzZXJ2aWNlLCAnY3JlYXRlJykubW9ja1JldHVyblZhbHVlKG9mKGVudGl0eSkpO1xuICAgICAgICAgIGNvbXAudXNlciA9IGVudGl0eTtcbiAgICAgICAgICAvLyBXSEVOXG4gICAgICAgICAgY29tcC5zYXZlKCk7XG4gICAgICAgICAgdGljaygpOyAvLyBzaW11bGF0ZSBhc3luY1xuXG4gICAgICAgICAgLy8gVEhFTlxuICAgICAgICAgIGV4cGVjdChzZXJ2aWNlLmNyZWF0ZSkudG9IYXZlQmVlbkNhbGxlZFdpdGgoZW50aXR5KTtcbiAgICAgICAgICBleHBlY3QoY29tcC5pc1NhdmluZykudG9FcXVhbChmYWxzZSk7XG4gICAgICAgIH0pXG4gICAgICApKTtcbiAgICB9KTtcbiAgfSk7XG59KTtcbiJdLCJ2ZXJzaW9uIjozfQ==