package com.mycompany.store.domain.enumeration;

/**
 * The SizeProduct enumeration.
 */
public enum SizeProduct {
    S,
    M,
    L,
    XL,
    XXL,
}
