ab35ec9c9a1a8b78939ae00c976d58c3
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
jest.mock('@ngx-translate/core');
const testing_1 = require("@angular/core/testing");
const testing_2 = require("@angular/common/http/testing");
const forms_1 = require("@angular/forms");
const rxjs_1 = require("rxjs");
const core_1 = require("@ngx-translate/core");
const error_constants_1 = require("app/config/error.constants");
const register_service_1 = require("./register.service");
const register_component_1 = require("./register.component");
describe('Component Tests', () => {
    describe('RegisterComponent', () => {
        let fixture;
        let comp;
        beforeEach(testing_1.waitForAsync(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [testing_2.HttpClientTestingModule],
                declarations: [register_component_1.RegisterComponent],
                providers: [forms_1.FormBuilder, core_1.TranslateService],
            })
                .overrideTemplate(register_component_1.RegisterComponent, '')
                .compileComponents();
        }));
        beforeEach(() => {
            fixture = testing_1.TestBed.createComponent(register_component_1.RegisterComponent);
            comp = fixture.componentInstance;
        });
        it('should ensure the two passwords entered match', () => {
            comp.registerForm.patchValue({
                password: 'password',
                confirmPassword: 'non-matching',
            });
            comp.register();
            expect(comp.doNotMatch).toBe(true);
        });
        it('should update success to true after creating an account', testing_1.inject([register_service_1.RegisterService, core_1.TranslateService], testing_1.fakeAsync((service, mockLanguageService) => {
            jest.spyOn(service, 'save').mockReturnValue(rxjs_1.of({}));
            mockLanguageService.currentLang = 'en';
            comp.registerForm.patchValue({
                password: 'password',
                confirmPassword: 'password',
            });
            comp.register();
            testing_1.tick();
            expect(service.save).toHaveBeenCalledWith({
                email: '',
                password: 'password',
                login: '',
                langKey: 'en',
            });
            expect(comp.success).toBe(true);
            expect(comp.errorUserExists).toBe(false);
            expect(comp.errorEmailExists).toBe(false);
            expect(comp.error).toBe(false);
        })));
        it('should notify of user existence upon 400/login already in use', testing_1.inject([register_service_1.RegisterService], testing_1.fakeAsync((service) => {
            jest.spyOn(service, 'save').mockReturnValue(rxjs_1.throwError({
                status: 400,
                error: { type: error_constants_1.LOGIN_ALREADY_USED_TYPE },
            }));
            comp.registerForm.patchValue({
                password: 'password',
                confirmPassword: 'password',
            });
            comp.register();
            testing_1.tick();
            expect(comp.errorUserExists).toBe(true);
            expect(comp.errorEmailExists).toBe(false);
            expect(comp.error).toBe(false);
        })));
        it('should notify of email existence upon 400/email address already in use', testing_1.inject([register_service_1.RegisterService], testing_1.fakeAsync((service) => {
            jest.spyOn(service, 'save').mockReturnValue(rxjs_1.throwError({
                status: 400,
                error: { type: error_constants_1.EMAIL_ALREADY_USED_TYPE },
            }));
            comp.registerForm.patchValue({
                password: 'password',
                confirmPassword: 'password',
            });
            comp.register();
            testing_1.tick();
            expect(comp.errorEmailExists).toBe(true);
            expect(comp.errorUserExists).toBe(false);
            expect(comp.error).toBe(false);
        })));
        it('should notify of generic error', testing_1.inject([register_service_1.RegisterService], testing_1.fakeAsync((service) => {
            jest.spyOn(service, 'save').mockReturnValue(rxjs_1.throwError({
                status: 503,
            }));
            comp.registerForm.patchValue({
                password: 'password',
                confirmPassword: 'password',
            });
            comp.register();
            testing_1.tick();
            expect(comp.errorUserExists).toBe(false);
            expect(comp.errorEmailExists).toBe(false);
            expect(comp.error).toBe(true);
        })));
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiL1VzZXJzL3RlbXAvRGVza3RvcC9KaGlwc3Rlcl9sZWFybm5pbmcvZS1jb21tZXJjZS1hcHAvc3JjL21haW4vd2ViYXBwL2FwcC9hY2NvdW50L3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0FBRWpDLG1EQUF5RztBQUN6RywwREFBdUU7QUFDdkUsMENBQTZDO0FBQzdDLCtCQUFzQztBQUN0Qyw4Q0FBdUQ7QUFFdkQsZ0VBQThGO0FBRTlGLHlEQUFxRDtBQUNyRCw2REFBeUQ7QUFFekQsUUFBUSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsRUFBRTtJQUMvQixRQUFRLENBQUMsbUJBQW1CLEVBQUUsR0FBRyxFQUFFO1FBQ2pDLElBQUksT0FBNEMsQ0FBQztRQUNqRCxJQUFJLElBQXVCLENBQUM7UUFFNUIsVUFBVSxDQUNSLHNCQUFZLENBQUMsR0FBRyxFQUFFO1lBQ2hCLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUF1QixDQUFDO2dCQUNsQyxZQUFZLEVBQUUsQ0FBQyxzQ0FBaUIsQ0FBQztnQkFDakMsU0FBUyxFQUFFLENBQUMsbUJBQVcsRUFBRSx1QkFBZ0IsQ0FBQzthQUMzQyxDQUFDO2lCQUNDLGdCQUFnQixDQUFDLHNDQUFpQixFQUFFLEVBQUUsQ0FBQztpQkFDdkMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FDSCxDQUFDO1FBRUYsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxzQ0FBaUIsQ0FBQyxDQUFDO1lBQ3JELElBQUksR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsK0NBQStDLEVBQUUsR0FBRyxFQUFFO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixRQUFRLEVBQUUsVUFBVTtnQkFDcEIsZUFBZSxFQUFFLGNBQWM7YUFDaEMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBRWhCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLHlEQUF5RCxFQUFFLGdCQUFNLENBQ2xFLENBQUMsa0NBQWUsRUFBRSx1QkFBZ0IsQ0FBQyxFQUNuQyxtQkFBUyxDQUFDLENBQUMsT0FBd0IsRUFBRSxtQkFBcUMsRUFBRSxFQUFFO1lBQzVFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwRCxtQkFBbUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixRQUFRLEVBQUUsVUFBVTtnQkFDcEIsZUFBZSxFQUFFLFVBQVU7YUFDNUIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLGNBQUksRUFBRSxDQUFDO1lBRVAsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDeEMsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLEtBQUssRUFBRSxFQUFFO2dCQUNULE9BQU8sRUFBRSxJQUFJO2FBQ2QsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsK0RBQStELEVBQUUsZ0JBQU0sQ0FDeEUsQ0FBQyxrQ0FBZSxDQUFDLEVBQ2pCLG1CQUFTLENBQUMsQ0FBQyxPQUF3QixFQUFFLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsZUFBZSxDQUN6QyxpQkFBVSxDQUFDO2dCQUNULE1BQU0sRUFBRSxHQUFHO2dCQUNYLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx5Q0FBdUIsRUFBRTthQUN6QyxDQUFDLENBQ0gsQ0FBQztZQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixRQUFRLEVBQUUsVUFBVTtnQkFDcEIsZUFBZSxFQUFFLFVBQVU7YUFDNUIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLGNBQUksRUFBRSxDQUFDO1lBRVAsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsd0VBQXdFLEVBQUUsZ0JBQU0sQ0FDakYsQ0FBQyxrQ0FBZSxDQUFDLEVBQ2pCLG1CQUFTLENBQUMsQ0FBQyxPQUF3QixFQUFFLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsZUFBZSxDQUN6QyxpQkFBVSxDQUFDO2dCQUNULE1BQU0sRUFBRSxHQUFHO2dCQUNYLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSx5Q0FBdUIsRUFBRTthQUN6QyxDQUFDLENBQ0gsQ0FBQztZQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO2dCQUMzQixRQUFRLEVBQUUsVUFBVTtnQkFDcEIsZUFBZSxFQUFFLFVBQVU7YUFDNUIsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLGNBQUksRUFBRSxDQUFDO1lBRVAsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FDSCxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsZ0NBQWdDLEVBQUUsZ0JBQU0sQ0FDekMsQ0FBQyxrQ0FBZSxDQUFDLEVBQ2pCLG1CQUFTLENBQUMsQ0FBQyxPQUF3QixFQUFFLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsZUFBZSxDQUN6QyxpQkFBVSxDQUFDO2dCQUNULE1BQU0sRUFBRSxHQUFHO2FBQ1osQ0FBQyxDQUNILENBQUM7WUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztnQkFDM0IsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLGVBQWUsRUFBRSxVQUFVO2FBQzVCLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNoQixjQUFJLEVBQUUsQ0FBQztZQUVQLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQ0gsQ0FBQyxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQyIsIm5hbWVzIjpbXSwic291cmNlcyI6WyIvVXNlcnMvdGVtcC9EZXNrdG9wL0poaXBzdGVyX2xlYXJubmluZy9lLWNvbW1lcmNlLWFwcC9zcmMvbWFpbi93ZWJhcHAvYXBwL2FjY291bnQvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNwZWMudHMiXSwic291cmNlc0NvbnRlbnQiOlsiamVzdC5tb2NrKCdAbmd4LXRyYW5zbGF0ZS9jb3JlJyk7XG5cbmltcG9ydCB7IENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQsIHdhaXRGb3JBc3luYywgaW5qZWN0LCB0aWNrLCBmYWtlQXN5bmMgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgSHR0cENsaWVudFRlc3RpbmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cC90ZXN0aW5nJztcbmltcG9ydCB7IEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgb2YsIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcblxuaW1wb3J0IHsgRU1BSUxfQUxSRUFEWV9VU0VEX1RZUEUsIExPR0lOX0FMUkVBRFlfVVNFRF9UWVBFIH0gZnJvbSAnYXBwL2NvbmZpZy9lcnJvci5jb25zdGFudHMnO1xuXG5pbXBvcnQgeyBSZWdpc3RlclNlcnZpY2UgfSBmcm9tICcuL3JlZ2lzdGVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgUmVnaXN0ZXJDb21wb25lbnQgfSBmcm9tICcuL3JlZ2lzdGVyLmNvbXBvbmVudCc7XG5cbmRlc2NyaWJlKCdDb21wb25lbnQgVGVzdHMnLCAoKSA9PiB7XG4gIGRlc2NyaWJlKCdSZWdpc3RlckNvbXBvbmVudCcsICgpID0+IHtcbiAgICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxSZWdpc3RlckNvbXBvbmVudD47XG4gICAgbGV0IGNvbXA6IFJlZ2lzdGVyQ29tcG9uZW50O1xuXG4gICAgYmVmb3JlRWFjaChcbiAgICAgIHdhaXRGb3JBc3luYygoKSA9PiB7XG4gICAgICAgIFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgICAgaW1wb3J0czogW0h0dHBDbGllbnRUZXN0aW5nTW9kdWxlXSxcbiAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtSZWdpc3RlckNvbXBvbmVudF0sXG4gICAgICAgICAgcHJvdmlkZXJzOiBbRm9ybUJ1aWxkZXIsIFRyYW5zbGF0ZVNlcnZpY2VdLFxuICAgICAgICB9KVxuICAgICAgICAgIC5vdmVycmlkZVRlbXBsYXRlKFJlZ2lzdGVyQ29tcG9uZW50LCAnJylcbiAgICAgICAgICAuY29tcGlsZUNvbXBvbmVudHMoKTtcbiAgICAgIH0pXG4gICAgKTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KFJlZ2lzdGVyQ29tcG9uZW50KTtcbiAgICAgIGNvbXAgPSBmaXh0dXJlLmNvbXBvbmVudEluc3RhbmNlO1xuICAgIH0pO1xuXG4gICAgaXQoJ3Nob3VsZCBlbnN1cmUgdGhlIHR3byBwYXNzd29yZHMgZW50ZXJlZCBtYXRjaCcsICgpID0+IHtcbiAgICAgIGNvbXAucmVnaXN0ZXJGb3JtLnBhdGNoVmFsdWUoe1xuICAgICAgICBwYXNzd29yZDogJ3Bhc3N3b3JkJyxcbiAgICAgICAgY29uZmlybVBhc3N3b3JkOiAnbm9uLW1hdGNoaW5nJyxcbiAgICAgIH0pO1xuXG4gICAgICBjb21wLnJlZ2lzdGVyKCk7XG5cbiAgICAgIGV4cGVjdChjb21wLmRvTm90TWF0Y2gpLnRvQmUodHJ1ZSk7XG4gICAgfSk7XG5cbiAgICBpdCgnc2hvdWxkIHVwZGF0ZSBzdWNjZXNzIHRvIHRydWUgYWZ0ZXIgY3JlYXRpbmcgYW4gYWNjb3VudCcsIGluamVjdChcbiAgICAgIFtSZWdpc3RlclNlcnZpY2UsIFRyYW5zbGF0ZVNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBSZWdpc3RlclNlcnZpY2UsIG1vY2tMYW5ndWFnZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2UpID0+IHtcbiAgICAgICAgamVzdC5zcHlPbihzZXJ2aWNlLCAnc2F2ZScpLm1vY2tSZXR1cm5WYWx1ZShvZih7fSkpO1xuICAgICAgICBtb2NrTGFuZ3VhZ2VTZXJ2aWNlLmN1cnJlbnRMYW5nID0gJ2VuJztcbiAgICAgICAgY29tcC5yZWdpc3RlckZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgcGFzc3dvcmQ6ICdwYXNzd29yZCcsXG4gICAgICAgICAgY29uZmlybVBhc3N3b3JkOiAncGFzc3dvcmQnLFxuICAgICAgICB9KTtcblxuICAgICAgICBjb21wLnJlZ2lzdGVyKCk7XG4gICAgICAgIHRpY2soKTtcblxuICAgICAgICBleHBlY3Qoc2VydmljZS5zYXZlKS50b0hhdmVCZWVuQ2FsbGVkV2l0aCh7XG4gICAgICAgICAgZW1haWw6ICcnLFxuICAgICAgICAgIHBhc3N3b3JkOiAncGFzc3dvcmQnLFxuICAgICAgICAgIGxvZ2luOiAnJyxcbiAgICAgICAgICBsYW5nS2V5OiAnZW4nLFxuICAgICAgICB9KTtcbiAgICAgICAgZXhwZWN0KGNvbXAuc3VjY2VzcykudG9CZSh0cnVlKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuZXJyb3JVc2VyRXhpc3RzKS50b0JlKGZhbHNlKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuZXJyb3JFbWFpbEV4aXN0cykudG9CZShmYWxzZSk7XG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pXG4gICAgKSk7XG5cbiAgICBpdCgnc2hvdWxkIG5vdGlmeSBvZiB1c2VyIGV4aXN0ZW5jZSB1cG9uIDQwMC9sb2dpbiBhbHJlYWR5IGluIHVzZScsIGluamVjdChcbiAgICAgIFtSZWdpc3RlclNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBSZWdpc3RlclNlcnZpY2UpID0+IHtcbiAgICAgICAgamVzdC5zcHlPbihzZXJ2aWNlLCAnc2F2ZScpLm1vY2tSZXR1cm5WYWx1ZShcbiAgICAgICAgICB0aHJvd0Vycm9yKHtcbiAgICAgICAgICAgIHN0YXR1czogNDAwLFxuICAgICAgICAgICAgZXJyb3I6IHsgdHlwZTogTE9HSU5fQUxSRUFEWV9VU0VEX1RZUEUgfSxcbiAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgICAgICBjb21wLnJlZ2lzdGVyRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICBwYXNzd29yZDogJ3Bhc3N3b3JkJyxcbiAgICAgICAgICBjb25maXJtUGFzc3dvcmQ6ICdwYXNzd29yZCcsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbXAucmVnaXN0ZXIoKTtcbiAgICAgICAgdGljaygpO1xuXG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yVXNlckV4aXN0cykudG9CZSh0cnVlKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuZXJyb3JFbWFpbEV4aXN0cykudG9CZShmYWxzZSk7XG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pXG4gICAgKSk7XG5cbiAgICBpdCgnc2hvdWxkIG5vdGlmeSBvZiBlbWFpbCBleGlzdGVuY2UgdXBvbiA0MDAvZW1haWwgYWRkcmVzcyBhbHJlYWR5IGluIHVzZScsIGluamVjdChcbiAgICAgIFtSZWdpc3RlclNlcnZpY2VdLFxuICAgICAgZmFrZUFzeW5jKChzZXJ2aWNlOiBSZWdpc3RlclNlcnZpY2UpID0+IHtcbiAgICAgICAgamVzdC5zcHlPbihzZXJ2aWNlLCAnc2F2ZScpLm1vY2tSZXR1cm5WYWx1ZShcbiAgICAgICAgICB0aHJvd0Vycm9yKHtcbiAgICAgICAgICAgIHN0YXR1czogNDAwLFxuICAgICAgICAgICAgZXJyb3I6IHsgdHlwZTogRU1BSUxfQUxSRUFEWV9VU0VEX1RZUEUgfSxcbiAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgICAgICBjb21wLnJlZ2lzdGVyRm9ybS5wYXRjaFZhbHVlKHtcbiAgICAgICAgICBwYXNzd29yZDogJ3Bhc3N3b3JkJyxcbiAgICAgICAgICBjb25maXJtUGFzc3dvcmQ6ICdwYXNzd29yZCcsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbXAucmVnaXN0ZXIoKTtcbiAgICAgICAgdGljaygpO1xuXG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yRW1haWxFeGlzdHMpLnRvQmUodHJ1ZSk7XG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yVXNlckV4aXN0cykudG9CZShmYWxzZSk7XG4gICAgICAgIGV4cGVjdChjb21wLmVycm9yKS50b0JlKGZhbHNlKTtcbiAgICAgIH0pXG4gICAgKSk7XG5cbiAgICBpdCgnc2hvdWxkIG5vdGlmeSBvZiBnZW5lcmljIGVycm9yJywgaW5qZWN0KFxuICAgICAgW1JlZ2lzdGVyU2VydmljZV0sXG4gICAgICBmYWtlQXN5bmMoKHNlcnZpY2U6IFJlZ2lzdGVyU2VydmljZSkgPT4ge1xuICAgICAgICBqZXN0LnNweU9uKHNlcnZpY2UsICdzYXZlJykubW9ja1JldHVyblZhbHVlKFxuICAgICAgICAgIHRocm93RXJyb3Ioe1xuICAgICAgICAgICAgc3RhdHVzOiA1MDMsXG4gICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICAgICAgY29tcC5yZWdpc3RlckZvcm0ucGF0Y2hWYWx1ZSh7XG4gICAgICAgICAgcGFzc3dvcmQ6ICdwYXNzd29yZCcsXG4gICAgICAgICAgY29uZmlybVBhc3N3b3JkOiAncGFzc3dvcmQnLFxuICAgICAgICB9KTtcblxuICAgICAgICBjb21wLnJlZ2lzdGVyKCk7XG4gICAgICAgIHRpY2soKTtcblxuICAgICAgICBleHBlY3QoY29tcC5lcnJvclVzZXJFeGlzdHMpLnRvQmUoZmFsc2UpO1xuICAgICAgICBleHBlY3QoY29tcC5lcnJvckVtYWlsRXhpc3RzKS50b0JlKGZhbHNlKTtcbiAgICAgICAgZXhwZWN0KGNvbXAuZXJyb3IpLnRvQmUodHJ1ZSk7XG4gICAgICB9KVxuICAgICkpO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9